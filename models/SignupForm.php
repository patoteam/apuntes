<?php
namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $user_name;
    public $user_email;
    public $user_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_name', 'filter', 'filter' => 'trim'],
            ['user_name', 'required'],
            ['user_name', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Este usuario ya esta en uso.'],
            ['user_name', 'string', 'min' => 2, 'max' => 255],

            ['user_email', 'filter', 'filter' => 'trim'],
            ['user_email', 'required'],
            ['user_email', 'email'],
            ['user_email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'este mail ya esta en uso'],

            ['user_password', 'required'],
            ['user_password', 'string', 'min' => 6],
        ];
    }
    public function attributeLabels()
    {
        return [
                'user_name' => 'Usuario',
              'user_password' => 'Contraseña',
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->user_name = $this->user_name;
            $user->user_email = $this->user_email;
            $user->setPassword($this->user_password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
