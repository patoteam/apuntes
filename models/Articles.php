<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property string $article_cod
 * @property integer $article_id
 * @property string $article_title
 * @property string $article_start
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public $article;

    public static function tableName()
    {
        return 'articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_cod', 'article_title','article_description'], 'required'],
            [['article_start'], 'safe'],
            [['article_cod'], 'string', 'max' => 4],
            [['article_title'], 'string', 'max' => 255],
            [['article_description'], 'string', 'max' => 4000],
            [['file'], 'safe'],
            [['file'], 'file', 'extensions'=>'jpg, gif, png, pdf, doc, docx, xls, xlsx, mp3', 'maxFiles'=>10000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_cod' => 'Article Cod',
            'article_id' => 'Article ID',
            'article_title' => 'Article Title',
            'article_start' => 'Article Start',
        ];
    }


    public function upload()
    {
        if ($this->file){
            foreach ($this->file as $one) {
                if(!file_exists(Yii::getAlias('@webroot')."/articles/".$this->article))
                    mkdir (Yii::getAlias('@webroot')."/articles/".$this->article);
                if(!file_exists(Yii::getAlias('@webroot')."/articles/".$this->article."/".$this->article_cod.".".$this->article_id))
                    mkdir (Yii::getAlias('@webroot')."/articles/".$this->article."/".$this->article_cod.".".$this->article_id);
                
                $one->saveAs(Yii::getAlias('@webroot')."/articles/".$this->article."/".$this->article_cod.".".$this->article_id."/". preg_replace('[\s+]','_', $one->baseName) . '.' . $one->extension);
            return true;
            }
        } else {
            return false;
        }
    }
}
