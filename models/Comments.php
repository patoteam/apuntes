<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article_comments".
 *
 * @property integer $comment_id
 * @property integer $article_code
 * @property integer $article_id
 * @property integer $parent_id
 * @property integer $comment_user_id
 * @property string $comment_text
 * @property string $comment_img
 * @property integer $comment_vote_up
 * @property integer $comment_vote_down
 * @property string $comment_datetime
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_code', 'article_id', 'parent_id', 'comment_user_id', 'comment_text', 'comment_img', 'comment_vote_up', 'comment_vote_down'], 'required'],
            [['article_code', 'article_id', 'parent_id', 'comment_user_id', 'comment_vote_up', 'comment_vote_down'], 'integer'],
            [['comment_datetime'], 'safe'],
            [['comment_text', 'comment_img'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => 'Comment ID',
            'article_code' => 'Article Code',
            'article_id' => 'Article ID',
            'parent_id' => 'Parent ID',
            'comment_user_id' => 'Comment User ID',
            'comment_text' => 'Comment Text',
            'comment_img' => 'Comment Img',
            'comment_vote_up' => 'Comment Vote Up',
            'comment_vote_down' => 'Comment Vote Down',
            'comment_datetime' => 'Comment Datetime',
        ];
    }
}
