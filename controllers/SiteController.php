<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\Articles;
use app\models\Logs;
use app\models\Comments;
use yii\helpers\Json;
use yii\db\Command;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
public function actionArticlelist($q=NULL)
{
    $out = [];
    $q=Yii::$app->request->get('q');
    if ($q)
    {
        $Articles = Articles::find()->where(['article_cod'=>'MAT'])->orWhere(['article_cod'=>'FACU'])->orWhere(['article_cod'=>'TERC'])->orWhere(['article_cod'=>'TECN'])->andWhere(['LIKE','article_title',$q])->limit(5)
->all();
        foreach ($Articles as $Article) {
            $query = new Query;
            $query->select('*')
                ->from('article_code')
                ->where('code ="' . $Article['article_cod'].'"');
            $command = $query->createCommand();
            $cod = $command->queryOne();
            $padre = Articles::find()->where(['article_cod'=>$Article['article_parent_cod'],'article_id'=>$Article['article_parent_id']])->one();
            $out[] = ['cod' => $cod['description'],'codid' => $cod['code'],'id' => $Article['article_id'],'value' => $Article['article_title'],'padre_title'=>$padre['article_title']];
        }
    }
    echo Json::encode($out);
}
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }   
private function getBreadcrumbs($parent_code,$parent_id,&$links)
{
    $Parent     = Articles::find()->where(['article_id'=>$parent_id,'article_cod'=>$parent_code])->One();
    if ($Parent){
        $this->getBreadcrumbs($Parent['article_parent_cod'],$Parent['article_parent_id'],$links);
        $links[]    =[  'label' => $Parent['article_nick'],
                        'url' => ['/site/childs',
                        'parent_code'=>$Parent['article_cod'],
                        'parent_id'=>$Parent['article_id']]
                    ];
    }
}
private function saveRow($page)
{
    $ipUser     =   $this->getUserIP();
    //$datetime   =   date("d-m-Y H:i:s");
    $log        =   new Logs();
    $log->ip    =   $ipUser;
    //$log->fecha =   $datetime;
    $log->page  =   $page;
    $log->save();

}
public function actionChilds($parent_code,$parent_id)
{
    $Parent         = Articles::find()->where(['article_id'=>$parent_id,'article_cod'=>$parent_code])->One();
    $Childrens      = Articles::find()->where(['article_parent_id'=>$parent_id,'article_parent_cod'=>$parent_code])->orderBy('article_start DESC')->all();
    $dataProvider   = new ArrayDataProvider([
                        'allModels' => $Childrens,
                        'pagination' => [
                            'pageSize' => 30,
                            ],
                    ]);  
    $breadcrumbs=[];

    $page= 'site/childs&parent_code='.$parent_code.'&parent_id='.$parent_id;
    $this->saveRow($page);
     $this->getBreadcrumbs($parent_code,$parent_id,$breadcrumbs);
    Yii::$app->session['breadcrumbs']=$breadcrumbs;
    if (Yii::$app->request->isAjax)
    return $this->renderAjax('childs', [
        'breadcrumbs'=>$breadcrumbs,
        'parent'    => $Parent,
    'dataProvider'  => $dataProvider,
    'UserIP' => $this->getUserIP()
    ]);
return $this->render('childs', [
        'breadcrumbs'=>$breadcrumbs,
        'parent'    => $Parent,
    'dataProvider'  => $dataProvider,
    'UserIP' => $this->getUserIP()
    ]);
}
public function actionReplyform($parent_id)
{
    echo $this->renderAjax('replyform',['parent'=>$parent_id]);
}
public function ParentsComments($parent_id)
{
    $comments          =   Comments::find()->where(['parent_id'=>$parent_id])->All();
    $dataProvider   = new ArrayDataProvider([
        'allModels' => $comments,
        'pagination' => [
            'pageSize' => 30,
            ],
    ]); 
    foreach ($dataProvider->models as $Comment)
    {
      echo  $this->renderAjax('comment',['Comment'=>$Comment]);
       $this->ParentsComments($Comment->comment_id);
    }
}
public function actionComments($parent_code,$parent_id)
    {
        $comments          =   Comments::find()->where(['article_code'=>$parent_code,'article_id'=>$parent_id,'parent_id'=>0])->All();
        $dataProvider   = new ArrayDataProvider([
                            'allModels' => $comments,
                            'pagination' => [
                                'pageSize' => 30,
                                ],
                        ]); 
         //echo $this->renderAjax('begincomments');
        echo  "<ul id='post-list' class='post-list'>";

        foreach ($dataProvider->models as $ParentComment)
        {
            echo $this->renderAjax('comment',['Comment'=>$ParentComment]);
            $this->ParentsComments($ParentComment->comment_id);
            echo  "</ul></li></ul>";
        }
        echo  "</ul></li></ul>";
            //return json_encode($dataProvider);  
        //return $this->renderAjax('comments',['dataProvider'=>$dataProvider]);     
    }
    public function actionUpload($parent_code,$parent_id)
    {
        $model          =   new Articles;
        //$parent_code    =   Yii::$app->request->get('article_code');
        //$parent_id      =   Yii::$app->request->get('article_id');

        if ($model->load(Yii::$app->request->post())) {
            $model->article_cod         =   'APTE';
            $model->article_parent_cod  =   $parent_code;
            $model->article_parent_id   =   $parent_id;
            $model->article_nick        =   'Apunte';
            if ($model->validate()) {
                $model->save();
                $model->article             =  $parent_code.".".$parent_id;
                $model->file                =   UploadedFile::getInstances($model, 'file');
                if ($model->upload()){
                    Yii::$app->getSession()->setFlash('success', 'Tu aporte fue ingresado con éxito.');
                    Yii::$app->session["parent_code"] = 'APTE';
                    Yii::$app->session["parent_id"] = $parent_id;
                    $model = new Articles;
                } else {
                    Yii::$app->getSession()->setFlash('error', 'No seleccionaste ningun Archivo!');
                    
                   
                    $again = new Articles;
                    $again->article_title         =  $model->article_title;
                    $again->article_description   =  $model->article_description;
                    $model->delete();
                    return $this->render('upload',['model'=>$again]);       
                }
            }
        }
    if (Yii::$app->request->isAjax)
        return $this->renderAjax('upload',['model'=>$model]);       
    return $this->render('upload',['model'=>$model]);       
     
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


public function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}


    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    public function  actionElmapt()
    {
        $apte_id = Yii::$app->request->post('id');
        $apte = Articles::find()->where(['article_cod'=>'APTE','article_id'=>$apte_id])->one();
        $data['parent_code']=$apte->article_parent_cod;
        $data['parent_id']=$apte->article_parent_id;
        $apte->delete();

        return json_encode($data);
    }
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

}
